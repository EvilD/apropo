module.exports = {
    siteMetadata: {
        title: `Apropo`,
        description: `Apropo is a proposal automation software dedicated for software development companies, giving a huge advantage over the competition.`,
        author: `@gatsbyjs`,
        testimonials: [
            {
                src: `person-1`,
                author: 'Przemek - CEO, The NorthStar Consulting Group',
                content: 'This is amazing. My team were spending so much time on quoting the inquiries which later on didn’t even turn into projects, and now it’s all the other way around - they are spending much less time on it, while we get much more deals closed each month..',
            },
            {
                src: `person_4`,
                author: 'Grzegorz - Developer',
                content: 'I am so grateful for this tool. I used to spend hours on preparing the list of features for every single inquiry and then on estimating it. Now I get a ready estimate for which I only need 5 minutes to validate and can get back to coding!'
            },
            {
                src: `person-2`,
                author: 'Stephan - Sales Specialist, Nogravity',
                content: 'It made my life so much easier and allowed me to close so much more deals. Before I came across Apropo, I were constantly fighting with the developers and pinging team few times per day about my estimate, while the client was already negotiating the terms with our competitors. Now it’s a different story - I don’t need developers anymore to prepare the quote and I’m able to provide it to the client within 1 hour!'
            },
        ],
        slider: [
            {
                src: `1`,
                title: 'ORGANIZE YOUR PROPOSALS ALL IN ONE PLACE',
                description: 'Gather all your proposals in one place and have a clear overview of who is involved in each of them',
            },
            {
                src: '2',
                title: 'CREATE NEW PROJECT',
                description: 'Create new proposal in a few seconds by specifying its name, work types and attaching the files',
            },
            {
                src: '3',
                title: 'CHOOSE THE SYSTEM TEMPLATE',
                description: 'Browse through the available apps & system estimate templates and pick the one closest to your project',
            },
            {
                src: '4',
                title: 'EDIT THE ESTIMATE BREAKDOWN',
                description: 'Add & remove features and modules and set the rules for automatic calculation of specific work types',
            },
            {
                src: '5',
                title: 'ESTIMATE WITH AI',
                description: 'Let our AI algorithm estimate the project or do it yourself',
            },
            {
                src: '6',
                title: 'SHARE THE PROPOSAL WITH THE CLIENT',
                description: 'Export the proposal or generate a shareable link allowing your customer to interact with the proposal in a browser and simulate various options',
            },
            {
                src: '7',
                title: 'SEE HOW THEY INTERACT WITH IT',
                description: 'See the videos showing how your clients interacted with the proposal',
            },
        ],
        pricing: [
            {
                title: "Estimates per month",
                free: "10",
                pro: "Unlimited",
                business: "Unlimited",
                enterprise: "Unlimited",
            },
            {
                title: "Users",
                free: "5",
                pro: "Unlimited",
                business: "Unlimited",
                enterprise: "Unlimited",
            },
            {
                title: "Credits for estimates templates",
                free: "$0.00",
                pro: "TBA",
                business: "TBA",
                enterprise: "TBA",
            },
            {
                title: "Estimating by AI",
                free: 'false',
                pro: 'true',
                business: 'true',
                enterprise: 'true',
            },
            {
                title: "Link sharing for proposals",
                free: 'false',
                pro: 'true',
                business: 'true',
                enterprise: 'true',
            },
            {
                title: "Session recording",
                free: 'false',
                pro: 'true',
                business: 'true',
                enterprise: 'true',
            },
            {
                title: "Own branding",
                free: 'false',
                pro: 'false',
                business: 'true',
                enterprise: 'true',
            },
            {
                title: "No watersign",
                free: 'false',
                pro: 'true',
                business: 'true',
                enterprise: 'true',
            },
            {
                title: "Own domain for proposals",
                free: 'false',
                pro: 'false',
                business: 'true',
                enterprise: 'true',
            },
            {
                title: "Document storage",
                free: '1 GB',
                pro: '5 GB',
                business: '10 GB',
                enterprise: 'TBA',
            },
            {
                title: "Premium support",
                free: 'false',
                pro: 'false',
                business: 'true',
                enterprise: 'true',
            }
        ]
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Apropo`,
                short_name: `Apropo`,
                start_url: `/`,
                background_color: `#663399`,
                theme_color: `#663399`,
                display: `minimal-ui`,
                icon: `src/images/favicon.png`,
            },
        },
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.dev/offline
        `gatsby-plugin-styled-components`,
        `gatsby-plugin-offline`,
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
                trackingId: "G-30RQCN8LBK",
            }
        },
    ],
}
