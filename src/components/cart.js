import React from "react"

import styled from 'styled-components'
import {Container} from "react-bootstrap";

const Wrapper = styled.div`
  background: ${props => props.dark ? 'rgba(68, 96, 207, 0.2)' : '#F8FAFF'};
  min-height: 364px;
  display: flex;
`

const InnerWrapper = styled.div`
  margin: auto;
  
  @media (max-width: 768px ) { 
      img{
        width: 35%;
      }
   }
   
   h1{
    font-size: 32px;
   }
   
   p{
    font-size: 18px;
   }
   
   @media (max-width: 768px) { 
    h1{
    font-size: 20px;
   }
   
   p{
    font-size: 16px;
   }
   img{
    margin-top: 25px;
    margin-bottom: 50px;
   }
  }
 
`

const Cart = ({children, dark}) => {

    return (
        <Wrapper dark={dark ? true : false}>
            <InnerWrapper className={'pt-md-0 pb-md-0 pb-5 m-auto'}>
                <Container>
                    {children}
                </Container>
            </InnerWrapper>
        </Wrapper>
    )
}

export default Cart