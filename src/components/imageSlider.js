import React from "react"
import SliderArrow from "../images/svg/sliderArrow.svg";
import {Carousel} from "react-bootstrap";
import {graphql, useStaticQuery} from "gatsby";
import styled, {css} from "styled-components";
import Decorator from "../images/svg/decorator.svg";

const H1 = styled.h1`
  font-size: 64px;
  
  @media (max-width: 768px) { 
    text-align: center;
    font-size: 32px !important;
    
    img{
      margin: auto;
      width: 32px;
      display:  block;
    }
  }
`

const TextDetail = styled.span`
  color: #435FCE;
  
  ${({decorator}) =>
    decorator &&
    css`
    &:before{
      position: absolute;
      top: -20px;
      content:"";
      background: url(${Decorator});
      background-size: cover;
      display: inline-block;
      width: 39px;
      height: 11px;
    }
    @media all and (max-width: 768px) { 
    &:before{
      top: -10px;
      width: 20px;
      height: 6px;
    }
   }
`
}
`

const CarouselStyle = styled(Carousel)`
  width: 70%;
  margin: auto;

  .carousel-indicators{
      position: absolute;
      bottom: 0;
      padding-bottom: 80px;
  }

  .carousel-indicators li{
      width: 16px;
      height: 16px;
      border-radius: 50%;
      background-color: #fff;
      margin: 8px;
  }
  
  .carousel-indicators li.active{
    background-color: #25CCD6;
    
  }
  
  .carousel-control-prev, .carousel-control-next{
    background-color: #435FCE;
    width: 60px;
    height: 60px;
    border-radius: 50%;
    margin: auto 0;
    box-shadow: 0 0 14px 2px rgba(0, 0, 0, 0.2);
  }
  
  @media all and (max-width: 768px ) {  
      width: 100%;
      
      .carousel-control-prev{
        left: 15px;
      }
      
      .carousel-control-next{
        right: 15px;
      }
      
      .carousel-item{
        padding: 0 !important;
      }
      
      .carousel-indicators{
        bottom: -40px;
     }
  }
  
`

const CarouselItemStyled = styled.div`
  position: relative;
  height: 68vh;
  margin: 15px 25px 0 25px;
  box-shadow: 0 0 14px 2px rgba(0, 0, 0, 0.2);
  border-radius: 20px;
  background-image: url(${props => props.image});
  background-repeat: no-repeat;
  background-size: 100%;
  
  @media only screen and (max-width: 768px ) {      
    margin: 15px 0 0 0;
    border-radius: 0 0 0 0;
    box-shadow: none;
    height: 610px;
    background-position: center;
    background-size: auto 100%;
  }
`

const CarouselItemContentStyled = styled.div`
  position: absolute;
  width: 100%;
  bottom: 0;
  height: 170px;
  color: #fff;
  text-align: center;
  border-radius:  0 0 20px 20px;
  background-color: rgba(67, 95, 206, 0.8);
  padding: 25px 0;
  
  p{
    color: #fff;
    font-size: 18px;
  }
  
  h5{
    color: #fff;
    font-size: 24px;
  }
  
  @media only screen and (max-width: 768px ) {
      min-height: 250px;
      border-radius: 0 0 0 0;
  }
`



const ImageSlider = () => {

    const data = useStaticQuery(graphql`
    query {
       site {
        siteMetadata {
          slider {
            src
            title
            description
          }
          testimonials{
            src
            author
            content
          }
        }
      }
      images: allFile(filter: {absolutePath: {regex: "/"}}) {
        edges {
          node {
            name
            publicURL
          }
        }
      }
    }
  `)

    return (
        <>
            <div>
                <H1 className={'mt-0 text-center'}><TextDetail>See</TextDetail> how it works!</H1>
            </div>
            <div className={'d-none d-md-block'}>
                <CarouselStyle fade nextIcon={<img src={SliderArrow} alt={SliderArrow}
                                                   style={{transform: 'rotate(-90deg)'}}/>}
                               prevIcon={<img src={SliderArrow} alt={SliderArrow}
                                              style={{transform: 'rotate(90deg)'}}/>}>
                    {
                        data.site.siteMetadata.slider.map((item) => {
                            return (<Carousel.Item className='mb-5 pb-5 pl-5 pr-5 text-left'>
                                <CarouselItemStyled image={
                                    data.images.edges.map((image) => {
                                        if (image.node.name === item.src) {
                                            return image.node.publicURL
                                        }
                                    })
                                }>
                                    <CarouselItemContentStyled>
                                        <h5>{item.title}</h5>
                                        <p className={'mt-3'}>{item.description}</p>
                                    </CarouselItemContentStyled>
                                </CarouselItemStyled>
                            </Carousel.Item>)
                        })
                    }
                </CarouselStyle>
            </div>
            <div className={'d-md-none d-block'}>
                <CarouselStyle fade nextIcon={<img src={SliderArrow} alt={SliderArrow}
                                                   style={{transform: 'rotate(-90deg)'}}/>}
                               prevIcon={<img src={SliderArrow} alt={SliderArrow}
                                              style={{transform: 'rotate(90deg)'}}/>}>

                    {
                        data.site.siteMetadata.slider.map((item) => {
                            return (<Carousel.Item className='mb-5 pb-5 pl-5 pr-5 text-left'>
                                <CarouselItemStyled image={
                                    data.images.edges.map((image) => {
                                        if (image.node.name === item.src) {
                                            return image.node.publicURL
                                        }
                                    })
                                }>
                                    <CarouselItemContentStyled>
                                        <h5>{item.title}</h5>
                                        <p className={'mt-3'}>{item.description}</p>
                                    </CarouselItemContentStyled>
                                </CarouselItemStyled>
                            </Carousel.Item>)
                        })
                    }
                </CarouselStyle>
            </div>
        </>
    )
}

export default ImageSlider