/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import {useStaticQuery, graphql, Link} from "gatsby"
import Img from "gatsby-image";
import styled from 'styled-components'

import "./layout.css"
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "./header"
import {Col, Container, Row} from "react-bootstrap";


const FooterLabel = styled.div`
  background-color: #393C51;
  color: #fff;
  font-size: 16px;
  line-height: 62px;
  min-height: 62px;
  
  a{
    color: #fff !important;
    display: inline-block;
    text-transform: uppercase;
  }
`

const Layout = ({children}) => {
    const data = useStaticQuery(graphql`
    query {
      footer: file(relativePath: { eq: "footer.png" }) {
         childImageSharp {
           fluid(maxWidth: 839) {
             ...GatsbyImageSharpFluid_noBase64
           }
         }
       }
    }
  `)

    return (
        <>
            <Header/>
            <div>
                <main>{children}</main>
                <footer className={'text-center'}>
                    <FooterLabel>
                        <Container>
                            <Row>
                                <Col xl={2} style={{color: '#fff'}}>Copyright ©2020</Col>
                                <Col xl={4} lg={0} ></Col>
                                <Col xl={6} lg={12}>
                                    <Row>
                                        {/*<Col xl={2} lg={2}  md={3}><a>HELP</a></Col>*/}
                                        <Col xl={3} lg={4}  md={3}><Link to={"/contact"}>Contact us</Link></Col>
                                        <Col xl={5} lg={4}  md={6}><a href={"https://app.apropo.io/terms"}>Terms and conditions</a></Col>
                                        <Col xl={4} lg={4} md={3}><a href={"https://app.apropo.io/policy"}>Privacy policy</a></Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                    </FooterLabel>
                    <Link to={"/european-funds"}><Img fluid={data.footer.childImageSharp.fluid} style={{maxWidth: '839px', position: 'relative', margin: 'auto'}}/></Link>
                </footer>
            </div>
        </>
    )
}

Layout.propTypes = {
    children: PropTypes.node.isRequired,
}

export default Layout
