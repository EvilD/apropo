import styled, {css} from "styled-components";
import Pattern from "../../images/pattern.png";
import Decorator from "../../images/svg/decorator.svg";
import {Table} from "react-bootstrap";

export const HeaderWrapper = styled.div`
  background-color: #F8FAFF;
  min-height: 470px;
  padding-top: 100px;
  text-align: center;
`

export const PatternWrapper = styled.div`
  min-height: 1000px;
  text-align: center;
  background-image: url(${Pattern});
  
  .responsive{
    position: relative;
    top: -200px;
  }
  
  @media all and (max-width: 1200px) { 
      .responsive{
        top: -130px;
       overflow-x:auto;
       margin: 0;
       max-width: 100%;
      }
  }
  
`

export const H1 = styled.h1`
  font-size: 64px !important;
  
  @media (max-width: 768px) { 
    text-align: center;
    font-size: 32px !important;
    
    img{
      margin: auto;
      width: 32px;
      display:  block;
    }
  }
`

export const TextDetail = styled.span`
  color: #435FCE;
  
  ${({decorator}) =>
    decorator &&
    css`
    &:before{
      position: absolute;
      top: -20px;
      content:"";
      background: url(${Decorator});
      background-size: cover;
      display: inline-block;
      width: 39px;
      height: 11px;
    }
    @media all and (max-width: 768px) { 
    &:before{
      top: -10px;
      width: 20px;
      height: 6px;
    }
   }
`
}
`

export const TableStyled = styled(Table)`
  margin-top: 15px;
  position: relative;
  margin-left: 15px;
  
  input[type=number]{
    display: inline-block;
    background-color: transparent;
    width: 60px;
    text-align: center;
    color: #435FCE;
    font-weight: 600;
    border: none;
    border-bottom: 2px solid #435FCE;
  }
  input[type=number]:focus{
    outline: none;
  }
  
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type=number] {
    -moz-appearance: textfield;
  }

 .mostPopular{
    background-color: #198FE7;
    color: #fff;
    height: 50px;
    border-radius: 20px 20px 0 0;
    box-shadow: 0 0 14px 2px rgba(0, 0, 0, 0.2);
  }
  
  .off-end {
    position: absolute;
    left: 100%;
    width: 15px;
    height: 1px;
    margin: 0;
    background: transparent none;
  }
  
  tfoot td{
    background-color: #fff;
  }
    
  thead th{
    background-color: #fff;
    border: none;
    margin-top: 10px;
    height: 300px;
    box-shadow: 0 0 7px 1px rgba(0, 0, 0, 0.2);
    clip-path: inset(-14px 0px 0 0px);
  }
  
  thead th:first-child{
    background-color: transparent;
    box-shadow: none;
  }
  
  thead th:nth-child(2){
    border-radius: 20px 0 0 0;
    clip-path: inset(-14px 0 0 -14px);
  }
  
  thead th:nth-child(3){
    clip-path: inset(-14px 0 0 0);
  }
  
  thead th:nth-child(4){
    clip-path: inset(0 0 0 0);
  }
  
  thead th:last-child{
    border-radius: 0 20px 0 0;
    clip-path: inset(-14px -14px 0 0);
  }
  
  td{
    border: none;
    background-color: #fff;
    min-width: 215px;
    max-width: 215px;
  }
  
  tbody{
   box-shadow: 0 0 14px rgba(0, 0, 0, 0.2);
   background-color: #fff;
  }
  
  
  tfoot td{
    box-shadow: 0 0 14px 2px rgba(0, 0, 0, 0.2);
    clip-path: inset(0 0 -14px 0);
  }
  
  tfoot td:first-child{
    background-color: transparent;
    box-shadow: none;
  }
  
  tfoot td:nth-child(2){
    border-radius: 0 0 0 20px;
    clip-path: inset(0 0 -14px -14px);

  }
  
  tfoot td:last-child{
    border-radius: 0  0 20px 0;
    clip-path: inset(0 -14px -14px 0);
  }
  
  & .popular{
    border-radius: 20px 20px 0 0;
    background-color: #198FE7;
    color: #fff;
    font-size: 14px;
    box-shadow: 0 0 14px rgba(0, 0, 0, 0.2);
    clip-path: inset( -14px -14px 0 -14px);
  }

 & .dolar{
    font-family: 'Roboto', sans-serif;
    font-weight: 700;
    font-size: 19px;
    margin-right: 5px;
 }
 
  & .price{
    font-family: 'Roboto', sans-serif;
    font-weight: 700;
    font-size: 32px;
 }
 
 & .link{
    text-decoration: none;
    font-size: 14px;
    color: rgba(67, 95, 206, 0.5);
    padding: 0 5px;
 }
 
 & .link-active{
    text-decoration: none;
    font-size: 14px;
    color: #435FCE;
    border-bottom: 2px solid #435FCE;
    padding: 0 5px;
 }
 
 & .free{
    border: 1px solid #FFF;
 }
 
  & .pro, & .enterprise{
    background-color: #F8FAFF;
    border: 1px solid #F8FAFF;
 }
 
 & .pro h2, & .enterprise h2{
    color: #435FCE;
 }
 
 & .business{
     background-color: #d1e9fa;
     border: 1px solid #d1e9fa;
 }
 
  & .business h2{
    color: #198FE7;
 }

input[type=range] {
  -webkit-appearance: none;
  margin: 10px 0;
  width: 100%;
  background: transparent;
}
input[type=range]:focus {
  outline: none;
}
input[type=range]::-webkit-slider-runnable-track {
  width: 100%;
  height: 4px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 0px 0px 0px #000000;
  background:  rgba(67, 95, 206, 0.5);
  border-radius: 2px;
  border: 0px solid #000000;
}
input[type=range]::-webkit-slider-thumb {
  box-shadow: 0px 0px 1px #000000;
  border: 0px solid #000000;
  height: 16px;
  width: 16px;
  border-radius: 8px;
  background: #435FCE;
  cursor: pointer;
  -webkit-appearance: none;
  margin-top: -6px;
}
input[type=range]:focus::-webkit-slider-runnable-track {
  background:  rgba(67, 95, 206, 0.5);
}
input[type=range]::-moz-range-track {
  width: 100%;
  height: 4px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 0px 0px 0px #000000;
  background: rgba(67, 95, 206, 0.5);
  border-radius: 2px;
  border: 0px solid #000000;
}
input[type=range]::-moz-range-thumb {
  box-shadow: 0px 0px 1px #000000;
  border: 0px solid #000000;
  height: 16px;
  width: 16px;
  border-radius: 8px;
  background: #435FCE;
  cursor: pointer;
}
input[type=range]::-ms-track {
  width: 100%;
  height: 4px;
  cursor: pointer;
  animate: 0.2s;
  background: transparent;
  border-color: transparent;
  color: transparent;
}
input[type=range]::-ms-fill-lower {
  background:  rgba(67, 95, 206, 0.5);
  border: 0px solid #000000;
  border-radius: 4px;
  box-shadow: 0px 0px 0px #000000;
}
input[type=range]::-ms-fill-upper {
  background:  rgba(67, 95, 206, 0.5);
  border: 0px solid #000000;
  border-radius: 4px;
  box-shadow: 0px 0px 0px #000000;
}
input[type=range]::-ms-thumb {
  box-shadow: 0px 0px 1px #000000;
  border: 0px solid #000000;
  height: 16px;
  width: 16px;
  border-radius: 8px;
  background: #435FCE;
  cursor: pointer;
}
input[type=range]:focus::-ms-fill-lower {
  background: rgba(67, 95, 206, 0.5);
}
input[type=range]:focus::-ms-fill-upper {
  background: rgba(67, 95, 206, 0.5);
}

`

export const ButtonFree = styled.div`
  text-transform: uppercase;
  font-weight: 600;
  display: flex;
  justify-content: center;
  color: #435FCE;
  height: 40px;
  width: 98px;
  line-height: 40px;
  border-radius: 10px;
  border: 1px solid #435FCE;
  
  &:hover, &:active{
    color: #fff;
    background-color: #435FCE;
    transition: color 0.2s, background-color 0.2s;
  }
`

export const ButtonPro = styled.div`
  text-transform: uppercase;
  font-weight: 600;
  display: flex;
  justify-content: center;
  color: #FFF;
  height: 40px;
  width: 98px;
  line-height: 40px;
  border-radius: 10px;
  border: 1px solid #435FCE;
  background-color: #435FCE;

`

export const ButtonBusiness = styled.div`
  text-transform: uppercase;
  font-weight: 600;
  display: flex;
  justify-content: center;
  color: #FFF;
  height: 40px;
  width: 98px;
  line-height: 40px;
  border-radius: 10px;
  border: 1px solid #198FE7;
  background-color: #198FE7;
`

export const ScrollBar = styled.div`
input[type=range] {
  -webkit-appearance: none;
  margin: 10px 0;
  width: 100%;
  background: transparent;
}
input[type=range]:focus {
  outline: none;
}
input[type=range]::-webkit-slider-runnable-track {
  width: 100%;
  height: 6px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 0px 0px 0px #000000;
  background: #9DACE6;
  border-radius: 0px;
  border: 0px solid #000000;
}
input[type=range]::-webkit-slider-thumb {
  box-shadow: 0px 0px 1px #000000;
  border: 0px solid #000000;
  height: 6px;
  width: 147px;
  border-radius: 1px;
  background: #435FCE;
  cursor: pointer;
  -webkit-appearance: none;
  margin-top: 0px;
}
input[type=range]:focus::-webkit-slider-runnable-track {
  background: #9DACE6;
}
input[type=range]::-moz-range-track {
  width: 100%;
  height: 6px;
  cursor: pointer;
  animate: 0.2s;
  box-shadow: 0px 0px 0px #000000;
  background: #9DACE6;
  border-radius: 0px;
  border: 0px solid #000000;
}
input[type=range]::-moz-range-thumb {
  box-shadow: 0px 0px 1px #000000;
  border: 0px solid #000000;
  height: 6px;
  width: 147px;
  border-radius: 1px;
  background: #435FCE;
  cursor: pointer;
}
input[type=range]::-ms-track {
  width: 100%;
  height: 6px;
  cursor: pointer;
  animate: 0.2s;
  background: transparent;
  border-color: transparent;
  color: transparent;
}
input[type=range]::-ms-fill-lower {
  background: #9DACE6;
  border: 0px solid #000000;
  border-radius: 0px;
  box-shadow: 0px 0px 0px #000000;
}
input[type=range]::-ms-fill-upper {
  background: #9DACE6;
  border: 0px solid #000000;
  border-radius: 0px;
  box-shadow: 0px 0px 0px #000000;
}
input[type=range]::-ms-thumb {
  box-shadow: 0px 0px 1px #000000;
  border: 0px solid #000000;
  height: 6px;
  width: 147px;
  border-radius: 1px;
  background: #435FCE;
  cursor: pointer;
}
input[type=range]:focus::-ms-fill-lower {
  background: #9DACE6;
}
input[type=range]:focus::-ms-fill-upper {
  background: #9DACE6;
}
`