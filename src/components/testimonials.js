import {graphql, useStaticQuery} from "gatsby";
import SliderArrow from "../images/svg/sliderArrow.svg";
import {Carousel} from "react-bootstrap";
import React from "react";
import styled, {css} from "styled-components";
import DecoratorUp from "../images/svg/decoratorComUp.svg";
import DecoratorDown from "../images/svg/decoratorComDown.svg";
import Decorator from "../images/svg/decorator.svg";

const H1 = styled.h1`
  font-size: 64px;
  
  @media (max-width: 768px) { 
    text-align: center;
    font-size: 32px !important;
    
    img{
      margin: auto;
      width: 32px;
      display:  block;
    }
  }
`

const TextDetail = styled.span`
  color: #435FCE;
  
  ${({decorator}) =>
    decorator &&
    css`
    &:before{
      position: absolute;
      top: -20px;
      content:"";
      background: url(${Decorator});
      background-size: cover;
      display: inline-block;
      width: 39px;
      height: 11px;
    }
    @media all and (max-width: 768px) { 
    &:before{
      top: -10px;
      width: 20px;
      height: 6px;
    }
   }
`
}
`

const CarouselTestimonialsStyle = styled(Carousel)`
  width: 60vw;
  margin: auto;
  font-size: 18px;
  
  .authorBox{
    position: absolute;
    bottom: 20px;
    margin-left: auto;
    margin-right: auto;
    left: 0;
    right: 0;
  }

  .carousel-indicators li{
      width: 16px;
      height: 16px;
      border-radius: 50%;
      background-color: rgba(57, 60, 81, 0.4);
      margin: 8px;
  }
  
  .carousel-indicators li.active{
    background-color: #25CCD6;
    
  }
  
  .carousel-control-prev, .carousel-control-next{
    display: none;
  }
  
  @media all and (max-width: 1000px) { 
      width: 85%;
       margin: auto;
      margin-bottom: 35px;
  }
  
 @media all and (max-width: 768px) { 
      width: 85%;
      margin: auto;
      margin-bottom: 35px;

      .carousel-item{
        padding: 0 !important;
      }
      
      .carousel-indicators{
        bottom: -15px;
     }
     
    &:before{
      position: absolute;
      top: -8px;
      left: 0;
      content:"";
      background: url(${DecoratorUp});
      background-size: cover;
      display: inline-block;
      width: 35px;
      height: 35px;
    }
    
    &:after{
      position: absolute;
      bottom: 25px;
      right: 0;
      content:"";
      background: url(${DecoratorDown});
      background-size: cover;
      display: inline-block;
      width: 35px;
      height: 35px;
    }
  }
  
  @media all and (min-width: 768px) { 
  &:before{
      position: absolute;
      top: -18px;
      left: 40px;
      content:"";
      background: url(${DecoratorUp});
      background-size: cover;
      display: inline-block;
      width: 35px;
      height: 35px;
    }
   &:after{
      position: absolute;
      bottom: 32px;
      right: 40px;
      content:"";
      background: url(${DecoratorDown});
      background-size: cover;
      display: inline-block;
      width: 35px;
      height: 35px;
   }
  }
}
  
`

const CarouselTestimonialsItemStyled = styled.div`
  position: relative;
  min-height: 350px;
  padding: 50px;
  padding-top: 20px;
  margin: 15px 25px 0 25px;
  box-shadow: 0 0 14px 2px rgba(0, 0, 0, 0.2);
  border-radius: 20px;
  
  @media only screen and (max-width: 1350px ) {
    height: 400px;
  }
  
  @media only screen and (max-width: 768px ) {     
    padding: 10px; 
    height: 450px;
    
    .content{
    display: block;
    display: -webkit-box;
    -webkit-line-clamp: 10;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    }
     
  }
`


const Testimonials = () => {

    const data = useStaticQuery(graphql`
    query {
       site {
        siteMetadata {
          slider {
            src
            title
            description
          }
          testimonials{
            src
            author
            content
          }
        }
      }
      images: allFile(filter: {absolutePath: {regex: "/"}}) {
        edges {
          node {
            name
            publicURL
          }
        }
      }
    }
  `)

    return (
        <>
            <H1 className={'text-center mt-5'}><TextDetail>Testimonials</TextDetail></H1>
            <CarouselTestimonialsStyle fade nextIcon={<img src={SliderArrow} alt={SliderArrow}
                                                           style={{transform: 'rotate(-90deg)'}}/>}
                                       prevIcon={<img src={SliderArrow} alt={SliderArrow}
                                                      style={{transform: 'rotate(90deg)'}}/>}>

                {
                    data.site.siteMetadata.testimonials.map((item) => {
                        var imageSRC = ''
                        data.images.edges.map((image) => {
                            if (image.node.name === item.src) {
                                imageSRC = image.node.publicURL
                            }
                        })
                        return (
                            <Carousel.Item className='mb-5 pb-4  pl-5 pr-5 text-center'>
                                <CarouselTestimonialsItemStyled>
                                    <p className={'mt-3 content'}>“{item.content}”</p>
                                    <div className={'authorBox'}>
                                        <img src={imageSRC} alt={imageSRC} className={'rounded-circle'}
                                             style={{width: '40px'}}/>
                                        <p className={'mt-3'}>
                                            {item.author}
                                        </p>
                                    </div>

                                </CarouselTestimonialsItemStyled>
                            </Carousel.Item>
                        )
                    })
                }
            </CarouselTestimonialsStyle>
        </>
    )
}

export default Testimonials
