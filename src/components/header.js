import {graphql, Link, useStaticQuery} from "gatsby"
import React, {useState} from "react"

import styled from 'styled-components'
import Img from "gatsby-image";
import Icon from "../images/svg/arrowButton.svg";
import Hamburger from "../images/svg/hamburger.svg";
import Exit from "../images/svg/exit.svg";
import Arrow from "../images/svg/arrow.svg";

const HeaderStyled = styled.header`
  height: 64px;
  background-color: #fff;
  box-shadow: 0 0 14px 2px rgba(71, 97, 225, 0.4);
  padding-right: 166px;
  padding-left: 166px;
  
  @media (max-width: 575.98px) { 
      padding-right: 20px;
      padding-left: 20px; 
   }
  
  @media (min-width: 576px) and (max-width: 1200px) { 
      padding-right: 70px;
      padding-left: 70px; 
   }
`

const HeaderLinksStyled = styled.div`
  height: 64px;
  float: right;
  text-transform: uppercase;
`
const HeaderLinkStyled = styled.div`
  font-size: 14px;
  display: inline-block;
  padding: 0 0 0 89px;  
  
  
  @media (min-width: 576px) and (max-width: 991.98px) { 
      padding: 0 0 0 25px;   
   }
  
  @media (min-width: 991.98px) and (max-width: 1200px) { 
      padding: 0 0 0 60px;   
   }
  
  
    & > img{
    position: relative;
    z-index: 100;
  }
  
`

const HeaderLinkMobileStyled = styled.div`
  font-size: 14px;
  display: inline-block;
  padding: 0 0 0 89px;  
  line-height: 64px;
  
  
  @media (min-width: 576px) and (max-width: 991.98px) { 
      padding: 0 0 0 40px;   
   }
  
  @media (min-width: 991.98px) and (max-width: 1200px) { 
      padding: 0 0 0 60px;   
   }
  
  
   & > img{
    position: relative;
    z-index: 100;
  }
  
`

const ButtonStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  text-transform: uppercase;

  background-color: #435FCE;
  height: 40px;
  width: 200px;
  border-radius: 10px;
  
  position: relative;
  
  margin-top: 12px;
  
  & p:before{
    content: url(${Icon});
    padding-right: 5px;
  }
  
  & p{
   color: #fff;
   font-weight: 600;
   margin: 0;
   position: absolute;              
   top: 50%;   
   left: 50%;                     
   margin-right: -50%;
   transform: translate(-50%, -50%)
  }
  
  &:hover{
    background-color: #FFF;
    transition: color 0.2s, background-color 0.2s;
    border-radius: 10px;
    border: 1px solid #435FCE;
  }
  
  &:hover p{
    color: #435FCE;
  }
  
`

const ButtonBorderStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 98px;
  border-radius: 10px;
  border: 1px solid #435FCE;
  
  position: relative;
 
  &:hover, &:active{
    background-color: #435FCE;
    transition: color 0.2s, background-color 0.2s;
  }
  
  &:hover p{
    color: #fff;
  }
  
  & p{
   color: #435FCE;
   margin: 0;
   position: absolute;              
   top: 50%;   
   left: 50%;                     
   margin-right: -50%;
   transform: translate(-50%, -50%)
  }
 
  
`

const ButtonLink = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 40px;
  width: 98px;
  
  position: relative;
  
  margin-top: 12px;
  
  & p{
    color: #393C51;
    text-decoration: none;
    font-family: muli, sans-serif;
    font-weight: 700;
    font-style: normal;
   margin: 0;
   position: absolute;              
   top: 50%;   
   left: 50%;                     
   margin-right: -50%;
   transform: translate(-50%, -50%)
  }
  
  & p:hover,& p:active{
    color: #435FCE;
    transition: color 0.2s;
  }
  
   @media (max-width: 767px) { 
        p{
            display: inline-block;
            color: #fff !important;
            font-weight: 500;
            margin-top: 10px;
        }
   }
  
`


const MobileNav = styled.div`
  background-color: #425ECE;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  clip-path: ${props => !props.toggleMenu ? 'circle(0px at 100% 0px)' : 'circle(100%)'} ;
  transition: clip-path 0.4s;
  
  padding-top: 25%;
  padding-left: 10%;
  color: #fff;
  z-index: 20;
`

const MobileNavItem = styled.div`
  margin-bottom: 20px;  
  a{
      display: inline-block;
      color: #fff;
      font-size: 32px;
      text-decoration: none;
      font-family: muli, sans-serif;
      font-weight: 700;
      font-style: normal;
      text-transform: none;
  }
  
  a > p{
    color: #fff !important;
  }
  
  a:hover{
    color: #fff;
  }
  
  &:before{
    content: url(${Arrow});
    padding-right: 15px;
  }
`

const MobileNavLogo = styled(Img)`
  position: fixed !important;
  right: -10px;
  top: 50%;
`

const MobileNavPattern = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  height: 90px;
  width: 100%;
  background-image: url(${props => props.pattern});
  background-size: 100%;
  background-repeat: repeat-x;
  z-index: 200;
  
  @media (min-width: 576px) and (max-width: 991.98px) { 
      height: 150px;  
   }
  
`


const Header = () => {
    const [toggleMenu, setToggleMenu] = useState(false)

    const data = useStaticQuery(graphql`
    query {
       apropo: file(relativePath: { eq: "apropo.png" }) {
         childImageSharp {
           fixed(height: 39) {
             ...GatsbyImageSharpFixed_noBase64
           }
         }
       }
       apropoMobile: file(relativePath: { eq: "apropoMobile.png" }) {
         childImageSharp {
           fixed(height: 170) {
             ...GatsbyImageSharpFixed_noBase64
           }
         }
       }
       apropoPattern: file(relativePath: { eq: "patternMenu.png" }) {
         publicURL
       }
    }
    `)

    return (
        <HeaderStyled className={'sticky-top'}>
            <div>
                <Link to="/">
                    <Img fixed={data.apropo.childImageSharp.fixed} style={{marginTop: '14px'}}/>
                </Link>
                <HeaderLinksStyled className={'d-none d-md-block'}>
                    <HeaderLinkStyled>
                        <Link to="/pricing">
                            <ButtonLink>
                                <p>Pricing</p>
                            </ButtonLink>
                        </Link>
                    </HeaderLinkStyled>
                    <HeaderLinkStyled>
                        <a href="https://app.apropo.io/register">
                            <ButtonStyled>
                                <p>Start Free Trial</p>
                            </ButtonStyled>
                        </a>
                    </HeaderLinkStyled>
                    <HeaderLinkStyled>
                        <a href="https://app.apropo.io/">
                            <ButtonBorderStyled>
                                <p>Sign In</p>
                            </ButtonBorderStyled>
                        </a>
                    </HeaderLinkStyled>
                </HeaderLinksStyled>
                <HeaderLinksStyled className={'d-md-none d-block'}>
                    <HeaderLinkMobileStyled>
                        {!toggleMenu ?
                            <img src={Hamburger} onClick={(e) =>setToggleMenu(true)} alt={Hamburger}/>
                            :
                            <img src={Exit} onClick={(e) => setToggleMenu(false)} alt={Exit} style={{position: 'fixed', right: '25px', top: '23px'}}/>
                        }
                        <MobileNav toggleMenu={toggleMenu}>
                            <MobileNavItem>
                                <Link to="/pricing">
                                    <ButtonLink>
                                        <p>Pricing</p>
                                    </ButtonLink>
                                </Link>
                            </MobileNavItem>
                            <MobileNavItem>
                                <a href="https://app.apropo.io/register">
                                    <p>Start Free Trial</p>
                                </a>
                            </MobileNavItem>
                            <MobileNavItem>
                                <a href="https://app.apropo.io/">
                                    <p>Sign In</p>
                                </a>
                            </MobileNavItem>
                            <MobileNavLogo fixed={data.apropoMobile.childImageSharp.fixed} />
                            <MobileNavPattern pattern={data.apropoPattern.publicURL}/>
                        </MobileNav>
                    </HeaderLinkMobileStyled>
                </HeaderLinksStyled>
            </div>
        </HeaderStyled>
    )

}

export default Header
