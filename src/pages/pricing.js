import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

import Disable from "../images/svg/disable.svg";
import Enable from "../images/svg/enable.svg";
import {Container} from "react-bootstrap";
import {graphql, StaticQuery} from "gatsby";
import {
    HeaderWrapper,
    PatternWrapper,
    H1,
    ButtonBusiness,
    ButtonFree,
    ButtonPro,
    ScrollBar,
    TableStyled,
    TextDetail
} from '../components/style/pricingStyle'

class Pricing extends React.Component {
    constructor(props) {
        super(props);
        this.tableRef = React.createRef();
        this.tableRefInner = React.createRef();
        this.tableRefNav = React.createRef();
        this.tableRefNavInner = React.createRef();
        this.scroll = this.scroll.bind(this);
        this.state = {
            ProValue: 5,
            ProValueToggle: true,
            BusinessValue: 5,
            BusinessValueToggle: true,
            EnterpriseValue: 5,
            EnterpriseValueToggle: true,
            Slider: 1,
            isHide: false,
            freePrice: 0.00,
            proPrice: 0.00,
            businessPrice: 0.00,
            enterprisePrice: 0.00,
            proBasePrice: 0.00,
            businessBasePrice: 0.00,
            enterpriseBasePrice: 0.00,
            proDiscount: 0.00,
            businessDiscount: 0.00,
            enterpriseDiscount: 0.00,
        }
    }

    componentWillMount() {
        this.setState({

            // Default Prices
            freePrice: 0.00,
            proBasePrice: 9.99,
            businessBasePrice: 9.99,
            enterpriseBasePrice: 9.99,

            //Product Discount - Annually
            proDiscount: 0.80,
            businessDiscount: 0.80,
            enterpriseDiscount: 0.80,


        }, () => {
            this.setState({
                proPrice: this.state.proBasePrice,
                businessPrice: this.state.businessBasePrice,
                enterprisePrice: this.state.enterpriseBasePrice,
            })
            this.updatePrices('proPrice', this.state.ProValue);
            this.updatePrices('businessPrice', this.state.BusinessValue);
            this.updatePrices('enterprisePrice', this.state.EnterpriseValue);
        })


    }

    updatePrices(stateName, value) {
        switch (stateName) {
            case 'proPrice':
                if (this.state.ProValueToggle === true) {
                    this.setState({[stateName]: this.state.proBasePrice * value * (1.00 - ((value - 1) * 0.05)) * this.state.proDiscount})
                } else {
                    this.setState({[stateName]: this.state.proBasePrice * value * (1.00 - ((value - 1) * 0.05))})
                }
                break;
            case 'businessPrice':
                if (this.state.BusinessValueToggle === true) {
                    this.setState({[stateName]: this.state.businessBasePrice * value * (1.00 - ((value - 1) * 0.05)) * this.state.businessDiscount})
                } else {
                    this.setState({[stateName]: this.state.businessBasePrice * value * (1.00 - ((value - 1) * 0.05))})
                }
                break;
            case 'enterprisePrice':
                if (this.state.EnterpriseValueToggle === true) {
                    this.setState({[stateName]: this.state.enterpriseBasePrice * value * (1.00 - ((value - 1) * 0.05)) * this.state.enterpriseDiscount})

                } else {
                    this.setState({[stateName]: this.state.enterpriseBasePrice * value * (1.00 - ((value - 1) * 0.05))})
                }
                break;
        }
    }

    scroll(value) {
        const total = this.tableRefInner.current.offsetWidth - window.innerWidth + 30;
        this.tableRef.current.scrollLeft = total * (value / 100);
    }

    hideBar = () => {
        const {isHide} = this.state

        if (window.scrollY > 400 && window.scrollY < 1200) {
            if (window.innerWidth < 992) {
                !isHide && this.setState({isHide: true})
            } else {
                isHide && this.setState({isHide: false});
            }
        } else {
            isHide && this.setState({isHide: false});
        }

        this.tableRefNav.current.scrollLeft = this.tableRef.current.scrollLeft
    }


    componentDidMount() {
        window.addEventListener('scroll', this.hideBar);
        this.tableRef.current.addEventListener('scroll', this.hideBar);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.hideBar);
        this.tableRef.current.removeEventListener('scroll', this.hideBar);
    }

    render() {

        return (
            <Layout>
                <SEO title="Pricing |"/>
                <HeaderWrapper>
                    <H1><TextDetail>Choose</TextDetail> a plan</H1>
                    <p style={{fontSize: '24px'}}>Give your team the features they need to succeed.</p>
                    <Container>
                        <ScrollBar className={'d-lg-none d-block text-left'}>
                            <p className={'mb-0'}>Scroll to see more</p>
                            <input type="range" min="1" value={this.state.Slider} step='0.01' max="100"
                                   style={{width: '100%'}}
                                   onChange={e => {
                                       this.scroll(e.target.value);
                                       this.setState({Slider: e.target.value})
                                   }}/>
                        </ScrollBar>
                    </Container>
                </HeaderWrapper>
                <PatternWrapper>
                    <Container className={'p-md-1 p-0 responsive'} ref={this.tableRef}>
                        <TableStyled ref={this.tableRefInner}>
                            <thead>
                            <tr>
                                <td colSpan={3} className={'d-none d-lg-table-cell'}
                                    style={{backgroundColor: 'transparent'}}></td>
                                <td colSpan={2} className={'d-lg-none d-table-cell'}
                                    style={{backgroundColor: 'transparent'}}></td>
                                <td className={'popular'}>
                                    Most popular
                                </td>
                            </tr>
                            <tr>
                                <th className={'d-none d-lg-block'}></th>
                                <th style={{paddingBottom: '160px', borderBottom: '1px solid #D8D8D8'}}
                                    className={'free'}>
                                    <h2 className={'mb-4'}>Free</h2>
                                    <h5 className={'mb-4'}><span className={'dolar'}>$</span><span
                                        className={'price'}>{this.state.freePrice.toFixed(2)}</span> / mo</h5>
                                </th>
                                <th className={'pro'} style={{borderBottom: '1px solid #D8D8D8'}}>
                                    <h2 className={'mb-4'}>Pro</h2>
                                    <h5 className={'mb-5'}><span className={'dolar'}>$</span><span
                                        className={'price'}>{this.state.proPrice.toFixed(2)}</span> / mo</h5>
                                    <p>for
                                        <input type="number" min={1} max={10} value={this.state.ProValue} step={1}
                                               onChange={e => {
                                                   this.setState({ProValue: e.target.value});
                                                   this.updatePrices('proPrice', e.target.value)
                                               }}/>
                                        user(s)
                                    </p>
                                    <input type="range" min="1" max="20" value={this.state.ProValue} step="1"
                                           style={{width: '70%'}} className={'mb-5'}
                                           onChange={e => {
                                               this.setState({ProValue: e.target.value});
                                               this.updatePrices('proPrice', e.target.value)
                                           }}/>
                                    <span className={this.state.ProValueToggle ? 'link' : 'link-active'}
                                          onClick={e => {
                                              this.setState({ProValueToggle: !this.state.ProValueToggle}, () => {
                                                  this.updatePrices('proPrice', this.state.ProValue)
                                              })
                                          }}>Monthly</span>
                                    <span className={!this.state.ProValueToggle ? 'link' : 'link-active'}
                                          onClick={e => {
                                              this.setState({ProValueToggle: !this.state.ProValueToggle}, () => {
                                                  this.updatePrices('proPrice', this.state.ProValue)
                                              })
                                          }}>Annually</span>
                                </th>
                                <th className={'business'} style={{borderBottom: '1px solid #D8D8D8'}}>
                                    <h2 className={'mb-4'}>Business</h2>
                                    <h5 className={'mb-5'}><span className={'dolar'}>$</span><span
                                        className={'price'}>{this.state.businessPrice.toFixed(2)}</span> / mo</h5>
                                    <p>for
                                        <input type="number" min={1} value={this.state.BusinessValue} step={1}
                                               onChange={e => {
                                                   this.setState({BusinessValue: e.target.value});
                                                   this.updatePrices('businessPrice', e.target.value)
                                               }}/>
                                        user(s)
                                    </p>
                                    <input type="range" min="1" max="20" value={this.state.BusinessValue} step="1"
                                           style={{width: '70%'}} className={'mb-5'}
                                           onChange={e => {
                                               this.setState({BusinessValue: e.target.value});
                                               this.updatePrices('businessPrice', e.target.value)
                                           }}/>
                                    <span className={this.state.BusinessValueToggle ? 'link' : 'link-active'}
                                          onClick={e => this.setState({BusinessValueToggle: !this.state.BusinessValueToggle}, () => {
                                              this.updatePrices('businessPrice', this.state.BusinessValue)
                                          })}>Monthly</span>
                                    <span className={!this.state.BusinessValueToggle ? 'link' : 'link-active'}
                                          onClick={e => this.setState({BusinessValueToggle: !this.state.BusinessValueToggle}, () => {
                                              this.updatePrices('businessPrice', this.state.BusinessValue)
                                          })}>Annually</span>
                                </th>
                                <th className={'enterprise'} style={{borderBottom: '1px solid #D8D8D8'}}>
                                    <h2 className={'mb-4'}>Enterprise</h2>
                                    <h5 className={'mb-5'}><span className={'dolar'}>$</span><span
                                        className={'price'}>{this.state.enterprisePrice.toFixed(2)}</span> / mo</h5>
                                    <p>for
                                        <input type="number" min={1} value={this.state.EnterpriseValue} step={1}
                                               onChange={e => {
                                                   this.setState({EnterpriseValue: e.target.value});
                                                   this.updatePrices('enterprisePrice', e.target.value)
                                               }}/>
                                        user(s)
                                    </p>
                                    <input type="range" min="1" max="20" value={this.state.EnterpriseValue}
                                           step="1"
                                           style={{width: '70%'}} className={'mb-5'}
                                           onChange={e => {
                                               this.setState({EnterpriseValue: e.target.value});
                                               this.updatePrices('enterprisePrice', e.target.value)
                                           }}/>
                                    <span className={this.state.EnterpriseValueToggle ? 'link' : 'link-active'}
                                          onClick={e => this.setState({EnterpriseValueToggle: !this.state.EnterpriseValueToggle}, () => {
                                              this.updatePrices('enterprisePrice', this.state.EnterpriseValue)
                                          })}>Monthly</span>
                                    <span className={!this.state.EnterpriseValueToggle ? 'link' : 'link-active'}
                                          onClick={e => this.setState({EnterpriseValueToggle: !this.state.EnterpriseValueToggle}, () => {
                                              this.updatePrices('enterprisePrice', this.state.EnterpriseValue)
                                          })}>Annually</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <StaticQuery
                                query={graphql`
                                    query{
                                      site { 
                                       siteMetadata {  
                                         pricing {
                                            title
                                            free
                                            pro
                                            business
                                            enterprise
                                         }
                                       } 
                                     }
                                    }
                                  `}
                                render={data => (
                                    data.site.siteMetadata.pricing.map((item) => {
                                        return (
                                            <>
                                                <tr className={'d-lg-none d-table-row '}>
                                                    <td colSpan={6} style={{border: '1px solid #D8D8D8'}}>
                                                        <div className={'text-center'}
                                                             style={{position: 'sticky', left: '0', width: '90vw'}}>
                                                            {item.title}
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style={{lineHeight: '50px'}}>
                                                    <td className={'d-none d-lg-table-cell text-left'} style={{
                                                        fontSize: '14px',
                                                        height: "75px",
                                                        border: '1px solid #D8D8D8',
                                                        minWidth: '270px',
                                                        maxWidth: '270px'
                                                    }}>
                                                        {item.title}
                                                    </td>
                                                    <td style={{height: "75px", border: '1px solid #D8D8D8'}}>
                                                        {item.free === 'true' ? <img src={Enable} alt={"Enable"}/> : ''}
                                                        {item.free === 'false' ?
                                                            <img src={Disable} alt={"Disable"}/> : ''}
                                                        {item.free !== 'true' && item.free !== 'false' ? item.free : ''}
                                                    </td>
                                                    <td style={{height: "75px", border: '1px solid #D8D8D8'}}
                                                        className={'pro'}>
                                                        {item.pro === 'true' ? <img src={Enable} alt={"Enable"}/> : ''}
                                                        {item.pro === 'false' ?
                                                            <img src={Disable} alt={"Disable"}/> : ''}
                                                        {item.pro !== 'true' && item.pro !== 'false' ? item.pro : ''}
                                                    </td>
                                                    <td style={{height: "75px", border: '1px solid #D8D8D8'}}
                                                        className={'business'}>
                                                        {item.business === 'true' ?
                                                            <img src={Enable} alt={"Enable"}/> : ''}
                                                        {item.business === 'false' ?
                                                            <img src={Disable} alt={"Disable"}/> : ''}
                                                        {item.business !== 'true' && item.business !== 'false' ? item.business : ''}
                                                    </td>
                                                    <td style={{height: "75px", border: '1px solid #D8D8D8'}}
                                                        className={'enterprise'}>
                                                        {item.enterprise === 'true' ?
                                                            <img src={Enable} alt={"Enable"}/> : ''}
                                                        {item.enterprise === 'false' ?
                                                            <img src={Disable} alt={"Disable"}/> : ''}
                                                        {item.enterprise !== 'true' && item.enterprise !== 'false' ? item.enterprise : ''}
                                                    </td>
                                                </tr>
                                            </>
                                        )

                                    })
                                )}
                            />
                            </tbody>
                            <tfoot>
                            <tr style={{height: "75px"}}>
                                <td className={'d-none d-lg-block'}></td>
                                <td><a href="https://app.apropo.io/register" style={{textDecoration: 'none'}}>
                                    <ButtonFree className={'m-auto w-100'}>
                                        Get started free
                                    </ButtonFree>
                                </a></td>
                                <td className={'pro'}><a href="https://app.apropo.io/register"
                                                         style={{textDecoration: 'none'}}>
                                    <ButtonPro className={'m-auto w-100'}>
                                        Choose a plan
                                    </ButtonPro>
                                </a></td>
                                <td className={'business'}><a href="https://app.apropo.io/register"
                                                              style={{textDecoration: 'none'}}>
                                    <ButtonBusiness className={'m-auto w-100'}>
                                        Choose a plan
                                    </ButtonBusiness>
                                </a></td>
                                <td className={'enterprise'}><a href="https://app.apropo.io/register"
                                                                style={{textDecoration: 'none'}}>
                                    <ButtonPro className={'m-auto w-100'}>
                                        Choose a plan
                                    </ButtonPro>
                                </a></td>
                            </tr>
                            </tfoot>
                            <div style={{
                                height: "75px",
                                width: '100%',
                                padding: '0 15px',
                                position: 'fixed',
                                bottom: '0',
                                left: '0',
                                zIndex: '100',
                                backgroundColor: '#fff',
                                overflow: 'hidden',
                                boxShadow: '0 0 14px 2px rgba(0, 0, 0, 0.2)'
                            }}
                                 className={!this.state.isHide ? 'd-none' : 'd-block'}
                                 ref={this.tableRefNav}
                            >
                                <div style={{width: '100%'}} ref={this.tableRefNavInner}>
                                    <td><a href="https://app.apropo.io/register" style={{textDecoration: 'none'}}>
                                        <ButtonFree className={'m-auto w-100'}>
                                            Get started free
                                        </ButtonFree>
                                    </a></td>
                                    <td className={'pro'}><a href="https://app.apropo.io/register"
                                                             style={{textDecoration: 'none'}}>
                                        <ButtonPro className={'m-auto w-100'}>
                                            Choose a plan
                                        </ButtonPro>
                                    </a></td>
                                    <td className={'business'}><a href="https://app.apropo.io/register"
                                                                  style={{textDecoration: 'none'}}>
                                        <ButtonBusiness className={'m-auto w-100'}>
                                            Choose a plan
                                        </ButtonBusiness>
                                    </a></td>
                                    <td className={'enterprise'}><a href="https://app.apropo.io/register"
                                                                    style={{textDecoration: 'none'}}>
                                        <ButtonPro className={'m-auto w-100'}>
                                            Choose a plan
                                        </ButtonPro>
                                    </a></td>
                                </div>
                            </div>
                            <div className={'off-end'}></div>
                        </TableStyled>
                    </Container>
                </PatternWrapper>
            </Layout>
        )
    }
}


export default Pricing
