import React from "react"
import {graphql, useStaticQuery} from "gatsby"

import Layout from "../components/layout"
import Img from "gatsby-image"
import SEO from "../components/seo"
import {Col, Container, Row, Carousel} from "react-bootstrap";
import styled, {css} from "styled-components";
import Icon from "../images/svg/thunder.svg";
import Cart from "../components/cart";

import Icon1 from '../images/svg/icon1.svg'
import Icon2 from '../images/svg/icon2.svg'
import Icon3 from '../images/svg/icon3.svg'
import Icon4 from '../images/svg/icon4.svg'
import Check from '../images/svg/check.svg'
import Decorator from '../images/svg/decorator.svg'
import Emote from '../images/svg/emote.svg'
import ArrowButton from "../images/svg/arrowButton.svg";
import DesktopMockup from '../images/svg/desktop-mockup.svg'
import ImageSlider from "../components/imageSlider";
import Testimonials from "../components/testimonials";
import Pattern from "../images/patternLite.png";

const ContainerStyled = styled(Container)`
  @media (max-width: 768px) { 
    p{
      font-size: 18px !important;
    }
  }
`

const H1 = styled.h1`
  font-size: 64px;
  
  @media (max-width: 768px) { 
    text-align: center;
    font-size: 32px !important;
    
    img{
      margin: auto;
      width: 32px;
      display:  block;
    }
  }
`

const TextDetail = styled.span`
  color: #435FCE;
  
  ${({decorator}) =>
    decorator &&
    css`
    &:before{
      position: absolute;
      top: -20px;
      content:"";
      background: url(${Decorator});
      background-size: cover;
      display: inline-block;
      width: 39px;
      height: 11px;
    }
    @media all and (max-width: 768px) { 
    &:before{
      top: -10px;
      width: 20px;
      height: 6px;
    }
   }
`
}
`

const ButtonStyled = styled.div`
  text-transform: uppercase;
  font-weight: 600;
  display: flex;
  justify-content: center;
  background-color: #435FCE;
  color: #FFF;
  height: 40px;
  width: 207px;
  line-height: 40px;
  text-transform: uppercase;
  border-radius: 10px;
  margin: 50px 0 0 0;
  
  &:before{
    content: url(${props => !props.arrow ? Icon : ArrowButton});
    padding-right: 5px;
    line-height:  ${props => !props.arrow ? '45px' : '40px'}
  }
  
  @media all and (max-width: 768px) { 
    margin: 30px auto 0 auto;
  }
  
`

const Ul = styled.ul`
  list-style: none;
  padding: 0;
  
  li{
      font-size: 25px;
  }
  
  li:before {
      content: url(${Check}); 
      padding-right: 10px;
  }

  @media all and (max-width: 768px) { 
    li{
          font-size: 16px;
      }
  }
`

export const PatternWrapper = styled.div`
  display: flex;
  min-height: 300px;
  background-image: url(${Pattern});
  background-repeat: repeat-x;
  background-position: 100% 250px;
  background-color: #F8FAFF;
  
   @media all and (max-width: 768px) { 
        background-position: 100% 470px;
    }
`

const IndexPage = () => {

    const data = useStaticQuery(graphql`
    query {
       apropoBig: file(relativePath: { eq: "apropoBig.png" }) {
         childImageSharp {
           fluid(maxHeight: 345, maxWidth: 417) {
             ...GatsbyImageSharpFluid_noBase64
           }
         }
       }
       site {
        siteMetadata {
          slider {
            src
            title
            description
          }
          testimonials{
            src
            author
            content
          }
        }
      }
      images: allFile(filter: {absolutePath: {regex: "/"}}) {
        edges {
          node {
            name
            publicURL
          }
        }
      }
    }
  `)


    return (
        <Layout>
            <SEO title=" "/>
            <ContainerStyled className={'mt-5 mb-5'}>
                <Row className={'pb-md-5 mb-md-5 mb-0 pb-0'}>
                    <Col md={6}>
                        <H1><TextDetail>Better</TextDetail> proposals.</H1>
                        <H1><TextDetail>Bigger</TextDetail> Sales.</H1>
                        <p className={'mt-5 text-center text-md-left'} style={{fontSize: '25px'}}>
                            Drive the sales in your software agency through the roof - prepare detailed and good looking
                            price proposals even 4 times faster and see how your customers interact with them!
                        </p>
                        <img src={DesktopMockup} alt={DesktopMockup} className={'d-md-none d-block w-75 m-auto'}/>
                        <ButtonStyled className={'mb-3'}>Start Free Trial</ButtonStyled>
                    </Col>
                    <Col md={6} className={'d-none d-md-block'}>
                        <img src={DesktopMockup} alt={DesktopMockup} className={'m-auto'} style={{width: '586px'}}/>
                    </Col>
                </Row>
            </ContainerStyled>
            <Container fluid style={{backgroundColor: '#F8FAFF'}}>
                <Container style={{paddingBottom: '20px', marginBottom: '50px'}}>
                    <Row className={'mt-5 pt-5'}>
                        <Col md={6} className={'d-none d-md-block'}>
                            <Img fluid={data.apropoBig.childImageSharp.fluid}
                                 style={{maxWidth: '300px'}}
                                 className={'m-auto'}/>
                        </Col>
                        <Col md={6} style={{marginTop: '0px'}}>
                            <H1><TextDetail>What</TextDetail> is Apropo?</H1>
                            <p className={'mt-5 text-center text-md-left'} style={{fontSize: '25px'}}>
                                Apropo is a proposal automation software dedicated for software development companies,
                                giving a huge advantage over the competition.
                            </p>
                        </Col>
                        <Col md={6} className={'d-md-none d-block'} style={{marginTop: '50px'}}>
                            <Img fluid={data.apropoBig.childImageSharp.fluid}
                                 style={{maxWidth: '417px'}}
                                 className={'w-50 m-auto'}/>
                        </Col>
                    </Row>
                </Container>
            </Container>

            <ImageSlider/>

            <H1 className={'text-center mb-5'} style={{fontSize: '48px'}}><TextDetail>What you
                get</TextDetail> from <br/> using Apropo</H1>
            <Cart>
                <Row className={'text-center text-md-left'}>
                    <Col md={6} className={'m-auto'}>
                        <h1><TextDetail decorator>Organize</TextDetail> your quoting process</h1>
                        <p>Gather all your company’s proposals in one place, edit them any time and see who of your team
                            members estimated each of them.</p>
                    </Col>
                    <Col md={6} className={'text-center order-first order-md-last'}>
                        <img src={Icon1} alt={Icon1}/>
                    </Col>
                </Row>
            </Cart>
            <Cart dark>
                <Row className={'text-center text-md-left'}>
                    <Col md={6} className={'text-center'}>
                        <img src={Icon2} alt={Icon2}/>
                    </Col>
                    <Col md={6} className={'m-auto'}>
                        <h1><TextDetail decorator>Save</TextDetail> time</h1>
                        <p>Use ready templates for most popular application & system types or build your own and make
                            your team spending even 8 times less time on breaking down the scope and estimating it.</p>
                    </Col>
                </Row>
            </Cart>
            <Cart>
                <Row className={'text-center text-md-left'}>
                    <Col md={6} className={'m-auto'}>
                        <h1><TextDetail decorator>Sell</TextDetail> more</h1>
                        <p>Prepare detailed and professionally looking proposals with your branding, share them with
                            client using shareable link and watch how they interact with it thanks to session
                            recording.</p>
                    </Col>
                    <Col md={6} className={'text-center order-first order-md-last'}>
                        <img src={Icon3} alt={Icon3}/>
                    </Col>
                </Row>
            </Cart>
            <Cart dark>
                <Row className={'text-center text-md-left'}>
                    <Col md={6} className={'text-center'}>
                        <img src={Icon4} alt={Icon4}/>
                    </Col>
                    <Col md={6} className={'m-auto'}>
                        <h1><TextDetail decorator>Sell with more profit</TextDetail></h1>
                        <p>Drive your sales through the roof - prepare detailed and good looking price proposals even 4
                            times faster and see how your customers interact with them!</p>
                    </Col>
                </Row>
            </Cart>

            <Testimonials/>

            <PatternWrapper>
                <Container className={'pt-md-0 pb-md-0 pb-5 m-auto'}>
                    <Row style={{marginBottom: '150px', marginTop: '80px'}}>
                        <Col md={6}>
                            <Ul>
                                <li>It’s free forever</li>
                                <li>No credit card required!</li>
                                <li>Decide on your own if and when you want to upgrade to business version</li>
                            </Ul>
                            <ButtonStyled arrow className={'d-md-none d-block text-center'}>Start Free
                                Trial</ButtonStyled>
                        </Col>
                        <Col md={6} className={'order-first order-md-last mb-5'}>
                            <H1 style={{fontSize: '64px'}}><TextDetail>Sign up</TextDetail> and see how it works <img
                                src={Emote}/></H1>
                            <ButtonStyled arrow className={'mt-5 d-none d-md-block text-center'}>Start Free
                                Trial</ButtonStyled>
                        </Col>
                    </Row>
                </Container>
            </PatternWrapper>


        </Layout>
    )
}

export default IndexPage
