import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import {Container} from "react-bootstrap";
import Img from "gatsby-image";
import {graphql, useStaticQuery} from "gatsby";

const EuropeanFunds = () => {
    const data = useStaticQuery(graphql`
    query {
      footer: file(relativePath: { eq: "footer.png" }) {
         childImageSharp {
           fluid(maxWidth: 839) {
             ...GatsbyImageSharpFluid_noBase64
           }
         }
       }
    }
  `)

    return (
        <Layout>
            <SEO title="European Funds"/>
            <Img fluid={data.footer.childImageSharp.fluid}
                 className={"mt-3"}
                 style={{maxWidth: '839px', position: 'relative', margin: 'auto'}}/>
            <Container>
                <p className="mt-5" style={{marginBottom: '30px', color: '#767470'}}>


                    The Company AI Estimo Software Sp. z o.o. implements project No. POPW.01.01.02-06-
                    0008 / 19, entitled: "Apropo - a tool facilitating the estimation of the value of IT projects
                    and improving business communication" under the Operational Program - Eastern
                    Poland 2014-2020, Priority Axis I: Entrepreneurial Eastern Poland, Activities 1.1 Starting
                    platforms for new ideas, Sub-measures 1.1.2 Development of startups in Eastern Poland.

                    <br/><br/>


                    <strong>Project goals:</strong> The main goal of the project is to create an Apropo platform
                    that
                    automates the costing process in software companies.
                    <br/>
                    <strong>Planned effects:</strong> The most important result planned to be achieved as a
                    result of the
                    project will be the development of a new Apropo product,the use of which will reduce
                    the amount of resources required to develop a valuation of software development
                    services, and thus contribute to improving the response to requests for quotation.
                    <br/>
                    <strong>Project value:</strong> PLN 1,115,844.00
                    <br/>
                    <strong>Contribution of European Funds:</strong> 929 504.00 PLN
                    <br/>
                    <strong>Implementation period:</strong> 01/01/2020 - 31/12/2021


                </p>

                <hr/>

                <p className="mb-5" style={{marginBottom: '30px', color: '#767470'}}>


                    Spółka AI Estimo Software Sp. z o.o. realizuje projekt nr POPW.01.01.02-06-0008/19, pn.:
                    „ Apropo – narzędzie ułatwiające szacowanie wartości projektów IT oraz usprawniające
                    komunikację biznesową” w ramach Programu Operacyjnego Polska Wschodnia 2014 –
                    2020, Osi Priorytetowej I: Przedsiębiorcza Polska Wschodnia, Działania 1.1 Platformy
                    startowe dla nowych pomysłów, Poddziałania 1.1.2 Rozwój startupów w Polsce
                    Wschodniej.

                    <br/><br/>

                    <strong>Cele projektu:</strong> Celem głównym projektu jest stworzenie platformy Apropo,
                    automatyzującej proces kosztorysowania w firmach tworzących oprogramowanie.
                    <br/>
                    <strong>Planowane efekty:</strong> Najważniejszym rezultatem, planowanym do osiągnięcia
                    w wyniku
                    realizacji projektu będzie opracowanie nowego produktu Apropo, którego
                    wykorzystanie pozwoli na zmniejszenie ilości zasobów potrzebnych do opracowania
                    wyceny usługi stworzenia oprogramowania, a tym samym przyczyni się do
                    usprawnienia odpowiedzi na zapytania ofertowe.
                    <br/>
                    <strong>Wartość projektu:</strong> 1 115 844,00 zł
                    <br/>
                    <strong>Wkład Funduszy Europejskich:</strong> 929 504,00 zł
                    <br/>
                    <strong>Okres realizacji:</strong> 01.01.2020 – 31.12.2021


                </p>


            </Container>
        </Layout>
    )

}

export default EuropeanFunds