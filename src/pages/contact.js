import React from "react"
import Layout from "../components/layout";
import SEO from "../components/seo";
import {Col, Container, Row} from "react-bootstrap";
import styled from "styled-components";
import SendIcon from "../images/svg/send-button.svg";
import {graphql, useStaticQuery} from "gatsby";
import Img from "gatsby-image";

const H1 = styled.h1`
  font-size: 52px;
  color: #435FCE;
  
  @media (max-width: 768px) { 
    text-align: center;
    font-size: 32px !important;
    
    img{
      margin: auto;
      width: 32px;
      display:  block;
    }
  }
`

const ButtonStyled = styled.button`
  text-transform: uppercase;
  font-weight: 600;
  display: flex;
  justify-content: center;
  background-color: #435FCE;
  color: #FFF;
  height: 40px;
  width: 50%;
  line-height: 30px;
  text-transform: uppercase;
  border-radius: 10px;
  margin: 50px 0 0 0;
  
  &:hover{
    color: #fff;
  }
  
  &:before{
    content: url(${SendIcon});
    line-height: 40px;
    padding-right: 10px;
  }
    
  @media all and (max-width: 768px) { 
    margin: 30px auto 0 auto;
    width: 80%;
  }
  
`

const ContactLink = styled.a`
    text-decoration: none;
    color: #435FCE;
    
    &:hover{
        text-decoration: none;
    }
`


const Contact = () => {
    const data = useStaticQuery(graphql`
    query {
       apropo: file(relativePath: { eq: "apropo.png" }) {
         childImageSharp {
           fixed(height: 39) {
             ...GatsbyImageSharpFixed_noBase64
           }
         }
       }
    }
    `)

    return (
        <Layout>
            <SEO title="Contact |"/>
            <Container className={'mb-5'}>
                <H1 className={'text-center mt-5'}>Contact</H1>
                <Row className={'mb-5 mt-5'} style={{marginBottom: '150px'}}>
                    <Col md={8} className={'m-auto'}>
                        <form>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Name</label>
                                <input className={'form-control'} type={'text'} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Email address</label>
                                <input className={'form-control'} type={'email'} required/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Phone number</label>
                                <input className={'form-control'} type={'tel'}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Company</label>
                                <input className={'form-control'} type={'text'}/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Message</label>
                                <textarea className={'form-control'} rows={'3'} required></textarea>
                            </div>
                            <ButtonStyled className={"btn mb-5"} type={'submit'}>Send Message</ButtonStyled>
                        </form>
                    </Col>
                    <Col md={3} className={'mt-md-0 mt-5 text-md-left text-center'}>
                        <Img fixed={data.apropo.childImageSharp.fixed} style={{marginTop: '14px'}}/>
                        <p className="mb-3 mt-3" style={{fontSize: '20px'}}>
                            <ContactLink href="mailto:michael@apropo.io">michael@apropo.io</ContactLink>
                        </p>
                        <p className="mb-0" >
                            <ContactLink href="tel:+48 660 766 911">+48 660 766 911</ContactLink>
                        </p>
                    </Col>
                </Row>
            </Container>
        </Layout>
    )
}

export default Contact